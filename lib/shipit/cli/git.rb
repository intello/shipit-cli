module Shipit
  module Cli
    class Git
      GIT     = ENV["SHIPIT_GIT"] || "git"
      ORIGIN  = ENV["SHIPIT_ORIGIN"] || "origin"
      VERBOSE = ENV["SHIPIT_GIT_VERBOSE"] ? "--verbose" : "--quiet"

      COMMANDS = {
        new: {
          desc: "create new branch `branch`",
          commands: [
            '"#{GIT} push #{ship_skips_ci}#{origin} #{current_branch}:refs/heads/#{branch} --no-verify #{VERBOSE}"',
            '"#{GIT} fetch #{origin} #{VERBOSE}"',
            '"#{GIT} branch --track #{branch} #{origin}/#{branch} #{VERBOSE}"',
            '"#{GIT} checkout #{branch} #{VERBOSE}"',
            '"#{GIT} commit --allow-empty -m \"#{message}\" #{VERBOSE}"',
            '"#{GIT} push #{ship_skips_ci}--no-verify #{VERBOSE}"'
          ]
        }
      }

      # Ex: Shipit::Cli::Git.run(command: :new, target_branch: "master", branch: "my-new-branch", message: "empty commit")
      def self.run(opt)
        if COMMANDS.keys.include?(opt[:command].to_sym)
          current_branch = opt[:target_branch]
          branch = opt[:branch]
          origin = ORIGIN
          message = opt.fetch(:message, "").gsub("`", "'")
          dry_run = opt.fetch(:dry_run, false)
          ship_skips_ci = "-o ci.skip " if Shipit::Cli.config.ship_skips_ci.to_s.downcase == "true"
          grepped_protected_branches = Array(Shipit::Cli.config.protected_branches)
            .push("master")
            .push("main")
            .push("develop")
            .push(get_current_branch)
            .uniq.map{ |b| "| grep -v #{b} " }.join
          COMMANDS[opt[:command].to_sym][:commands].map do |x|
            command = exec_cmd(eval(x), dry_run)
            exit_now!("There was an error running the last Git command. See the trace for more info.") if dry_run == false && command == false
          end
        else
          false
        end
      end

      def self.get_current_branch
        (`#{GIT} branch 2> /dev/null | grep '^\*'`).gsub(/[\*\s]/, "")
      end

      def self.exec_cmd(str, dry_run = false)
        return true unless str
        if dry_run
          puts "    - #{str}"
        else
          system("#{str}")
        end
      end
    end
  end
end
