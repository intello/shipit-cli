require "yaml"

module Shipit
  module Cli
    class ConfigurationFile
      ATTR_READER = [:endpoint, :private_token, :protected_branches, :ship_skips_ci]
      attr_reader(*ATTR_READER)

      def initialize(path, configuration = nil)
        @path = path
        @file = load_file
        @configuration = configuration || load_configuration
      end

      def exist?
        File.exist?(@path)
      end

      def to_hash
        config_hash = ATTR_READER.inject({}) do |hash, attr|
          hash["#{attr}"] = instance_variable_get("@#{attr}")
          hash
        end
        Shipit::Cli::Sanitizer.symbolize config_hash
      end

      def persist
        File.open(@path, "w") do |f|
          f.write @configuration.to_yaml
        end
        reload!
      end

      def reload!
        @file = load_file
        @configuration = load_configuration
      end

      private

      def load_file
        if exist?
          Shipit::Cli::Sanitizer.symbolize YAML.load_file(@path)
        else
          {}
        end
      end

      def load_configuration
        if @file
          ATTR_READER.each do |attr|
            instance_variable_set "@#{attr}", @file[attr]
          end
        end
      end
    end
  end
end
