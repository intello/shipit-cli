# Changelog

All notable changes to this project will be documented in this file.

NOTE: the project follows [Semantic Versioning](http://semver.org/).

## v1.3.1 - August 31st, 2023

- [bug] #5 - Does not like when labels are of form bug1foobar scoped labels

## v1.3.0 - October 12th, 2022

- [enhancement] #8 - Add option to skip CICD when shipping

## v1.2.0 - March 9th, 2022

- [bug] #3 - Support when project is located in sub folder

## v1.1.0 - February 7th, 2022

- [enhancement] #5 - Run shipit from a Docker environement for nonLinux environement
- [task] #1 - Support gitlab.com new Draft status

## v1.0.0 - November 25th, 2021

- Inivial version

