# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "shipit/cli/version"

Gem::Specification.new do |spec|
  spec.name                  = "intello-shipit-cli"
  spec.version               = Shipit::Cli::VERSION
  spec.authors               = ["Benjamin Thouret"]
  spec.email                 = ["benjamin.thouret@intello.com"]
  spec.licenses              = ["MIT"]
  spec.description           = %q{Designed to simplify working with Gitlab, its main purpose is to kickstart working on an issue.}
  spec.summary               = %q{Designed to simplify working with Gitlab, its main purpose is to kickstart working on an issue.}
  spec.homepage              = "https://gitlab.com/intello/shipit-cli"
  spec.required_ruby_version = ">= 2.5.0"

  spec.files                 = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir                = "exe"
  spec.executables           = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths         = ["lib"]

  spec.add_dependency "activesupport"
  spec.add_dependency "gli", "~> 2.13"
  spec.add_dependency "gitlab", "~> 4.3.0"
  spec.add_dependency "git_clone_url", "~> 2.0.0"

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "guard"
  spec.add_development_dependency "guard-ctags-bundler"
  spec.add_development_dependency "guard-rspec"
  spec.add_development_dependency "guard-rubocop"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rubocop"
  spec.add_development_dependency "simplecov"
end
